//
// Created by hrd on 16/01/2022.
//

#include "../include/file.h"

enum file_access_status file_open(FILE** file, const char* filename, const char* open_mode) {
    *file = fopen(filename, open_mode);
    if (*file) return FILE_OK;
    else return FILE_OPEN_ERROR;
}
enum file_access_status file_close(FILE** file) {
    if (fclose(*file)) {
        return FILE_CLOSE_ERROR;
    } else return FILE_OK;
}
