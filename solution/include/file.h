//
// Created by hrd on 16/01/2022.
//

#ifndef IMG_ROT_FILE_H
#define IMG_ROT_FILE_H

#include <stdio.h>

enum file_access_status {
    FILE_OK = 0,
    FILE_OPEN_ERROR,
    FILE_CLOSE_ERROR
};

enum file_access_status file_open(FILE** file, const char* filename, const char* open_mode);
enum file_access_status file_close(FILE** file);

#endif //IMG_ROT_FILE_H
