//
// Created by hrd on 16/01/2022.
//

#include "../include/image.h"

struct image create_image(const uint64_t width, const uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc( width * height * sizeof(struct pixel) )
    };
}

void free_image(struct image* image) {
    free(image->data);
}

struct pixel get_pixel(const struct image image, const size_t x, const size_t y) {
    if (x < image.width && y < image.height) {
        return image.data[y*image.width + x];
    } else {
        return (struct pixel){0, 0, 255};
    }
}

void set_row(const struct pixel* row, const size_t row_n, const struct image image) {
    for (size_t i = 0; i < image.width; i++) {
        set_pixel(image, i, row_n, row[i]);
    }
}

void set_pixel(const struct image image, const size_t x, const size_t y, const struct pixel pixel) {
    if (x < image.width && y < image.height) {
        image.data[y*image.width + x] = pixel;
    }
}
