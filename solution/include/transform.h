//
// Created by hrd on 16/01/2022.
//

#ifndef IMG_ROT_TRANSFORM_H
#define IMG_ROT_TRANSFORM_H

#include "image.h"

void rotate_90deg(const struct image source, struct image* target);

#endif //IMG_ROT_TRANSFORM_H
