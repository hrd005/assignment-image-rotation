//
// Created by hrd on 16/01/2022.
//

#include "transform.h"

void rotate_90deg(const struct image source, struct image* target) {
    *target = create_image(source.height, source.width);

    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            struct pixel pixel = get_pixel(source, x, y);
            set_pixel(*target, source.height - 1 - y, x, pixel);
        }
    }
}
