//
// Created by hrd on 16/01/2022.
//

#include "../include/bmp.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status from_bmp(FILE* source, struct image* target) {
    struct bmp_header header = {0};
    const size_t isHeaderRead = fread(&header, sizeof (struct bmp_header), 1, source);
    if (!isHeaderRead) {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != 24)
        return READ_INVALID_HEADER;

    *target = create_image(header.biWidth, header.biHeight);

    for (size_t i = 0; i < target->height; i++) {
        
        struct pixel* row = &target->data[target->width*i];
        const size_t readPixels = fread(row, sizeof (struct pixel), target->width, source);

         if (readPixels != target->width) {
             free_image(target);
             return READ_INVALID_BITS;
         }

         int64_t padding = target->width % 4;
         int64_t isNotPadded = 0;
         if (padding) isNotPadded = fseek(source, padding, SEEK_CUR);
         if (isNotPadded) {
             free_image(target);
             return READ_INVALID_BITS;
         }
    }

    return READ_OK;
}

static struct bmp_header create_header(const struct image image);

enum write_error to_bmp(const struct image source, FILE* target) {
    struct bmp_header header = create_header(source);
    const int64_t isHeaderWritten = fwrite(&header, sizeof (struct bmp_header), 1, target);
    if (!isHeaderWritten) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < source.height; i++) {
        const int64_t isRowWritten =
                fwrite(source.data + i * source.width, sizeof (struct pixel), source.width, target);
        if (!isRowWritten) {
            return WRITE_ERROR;
        }

        int64_t padding = source.width % 4;
        int64_t isPadded = 0;
        if (padding) isPadded = fwrite(&isRowWritten, 1, padding, target);
        if (!isPadded) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

static struct bmp_header create_header(const struct image image) {
    const uint32_t header_size = sizeof (struct bmp_header);
    const uint32_t image_size = sizeof (struct pixel) * image.height * (image.width + image.width % 4);

    return (struct bmp_header) {
        .bfType = 0x4D42,
        .bfileSize = header_size + image_size,
        .bfReserved = 0,
        .bOffBits = header_size,
        .biSize = 40,
        .biWidth = image.width,
        .biHeight = image.height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}
