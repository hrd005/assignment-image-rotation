//
// Created by hrd on 16/01/2022.
//

#ifndef IMG_ROT_IMAGE_H
#define IMG_ROT_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void set_row(const struct pixel* row, const size_t row_n, const struct image image);

struct pixel get_pixel(const struct image image, const size_t x, const size_t y);
void set_pixel(const struct image image, const size_t x, const size_t y, const struct pixel pixel);

struct image create_image(const uint64_t width, const uint64_t height);

void free_image(struct image* image);

#endif //IMG_ROT_IMAGE_H
