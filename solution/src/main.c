#include "file.h"
#include "bmp.h"
#include "transform.h"
#include <stdio.h>

int my_exit(FILE* input, FILE* output, int errcode) {
    if (input) {
        enum file_access_status fis = file_close(&input);
        if (fis != FILE_OK) return -1;
    }
    if (output) {
        enum file_access_status fos = file_close(&output);
        if (fos != FILE_OK) return -1;
    }

    return errcode;
}

int main( int argc, char** argv ) {

    if (argc < 2) {
        return 1;       // Not enough args
    }

    FILE* input_file;
    enum file_access_status fis = file_open(&input_file, argv[1], "r");
    if (fis != FILE_OK) {
        fprintf(stderr, "Unable to read source file.");
        return my_exit(input_file, NULL, 2);
    }

    FILE* output_file;
    enum file_access_status fos = file_open(&output_file, argv[2], "w");
    if (fos != FILE_OK) {
        fprintf(stderr, "Unable to write to target file.");
        return my_exit(input_file, output_file, 3);
    }

    struct image source = {0};
    enum read_status irs = from_bmp(input_file, &source);
    if (irs != READ_OK) {
        free_image(&source);
        fprintf(stderr, "Unable to parse inputted image.");
        return my_exit(input_file, output_file, 4);
    }

    struct image target = {0};
    rotate_90deg(source, &target);

    enum write_error ows = to_bmp(target, output_file);
    if (ows != WRITE_OK) {
        free_image(&source);
        free_image(&target);
        fprintf(stderr, "Unable to combine image.");
        return my_exit(input_file, output_file, 5);
    }

    free_image(&source);
    free_image(&target);
    return my_exit(input_file, output_file, 0);
}

