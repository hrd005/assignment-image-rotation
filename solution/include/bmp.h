//
// Created by hrd on 16/01/2022.
//

#ifndef IMG_ROT_BMP_H
#define IMG_ROT_BMP_H

#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status from_bmp(FILE* source, struct image* target);

enum write_error {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_error to_bmp(const struct image source, FILE* target);

#endif //IMG_ROT_BMP_H
